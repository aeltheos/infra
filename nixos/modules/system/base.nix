{
  self,
  config,
  pkgs,
  impermanence,
  ...
}:
{
  imports = [ impermanence.nixosModules.impermanence ];

  system.stateVersion = "24.05";

  networking.useDHCP = false;

  nix = {
    optimise.automatic = true;
    gc.automatic = true;
    extraOptions = ''
      experimental-features = flakes nix-command
    '';
  };

  # Disable users and make ssh access immutable.
  services.openssh = {
    enable = true;

    authorizedKeysInHomedir = false;
    settings.PasswordAuthentication = false;
  };

  # Locale
  i18n.defaultLocale = "en_US.UTF-8";
  console.keyMap = "fr";
  time.timeZone = "Europe/Paris";

  # Minimal admin utilities.
  programs.git.enable = true;
  programs.tmux.enable = true;
  environment.enableAllTerminfo = true;
  environment.systemPackages = [ pkgs.helix ];

  services.pcscd.enable = true;
  security.tpm2 = {
    enable = true;
    pkcs11.enable = true;
    tctiEnvironment.enable = true;
  };

  # Immutable users.
  users.mutableUsers = false;

  age.secrets.rootPasswdHash = {
    rekeyFile = self + "/secrets/master/hosts/${config.networking.hostName}/root.age";
    generator.script = "passphrase";
  };

  users.users.root = {
    hashedPasswordFile = config.age.secrets.rootPasswdHash.path;
    openssh.authorizedKeys.keyFiles = [
      (self + "/keys/tokens/yk19666063/id_ecdsa_sha2_nistp384.pub")
      (self + "/keys/tokens/yk19666099/id_ecdsa_sha2_nistp384.pub")
    ];
  };

  # impermanent root.
  fileSystems."/" = {
    device = "none";
    fsType = "tmpfs";
    options = [
      "size=25%"
      "mode=755"
    ];
  };

  environment.persistence."/mnt/persist-sys" = {
    enable = true;
    directories = [
      "/var/log"
      "/var/lib/nixos"
      "/etc/secureboot"
      "/root"
      "/etc/nixos"
    ];
  };

  fileSystems."/etc/ssh" = {
    device = "/mnt/persist-sys/etc/ssh";
    options = [ "bind" ];
    neededForBoot = true;
  };
}

{
  self,
  config,
  agenix,
  agenix-rekey,
  ...
}:
{
  imports = [
    agenix.nixosModules.default
    agenix-rekey.nixosModules.default
  ];

  age.rekey = {
    hostPubkey = builtins.readFile (
      self + "/keys/hosts/${config.networking.hostName}/ssh_host_ed25519_key.pub"
    );

    masterIdentities = [
      (self + /keys/tokens/yk19666063/age-identity.pub)
      (self + /keys/tokens/yk19666099/age-identity.pub)
    ];
    storageMode = "local";

    localStorageDir = self + "/secrets/rekeys/${config.networking.hostName}";
  };
}

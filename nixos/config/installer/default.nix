{ pkgs, inputs }:
(inputs.nixpkgs.lib.nixosSystem {
  specialArgs = inputs;
  modules = [
    (inputs.nixpkgs + "/nixos/modules/installer/cd-dvd/installation-cd-minimal.nix")
    { nixpkgs.hostPlatform = pkgs.stdenv.system; }
    ./configuration.nix
  ];
}).config.system.build.isoImage

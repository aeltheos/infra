{ self, pkgs, ... }:
{
  nix.extraOptions = ''
    experimental-features = flakes nix-command
  '';

  programs.git.enable = true;
  programs.tmux.enable = true;
  environment.enableAllTerminfo = true;
  environment.systemPackages = [ pkgs.helix ];

  services.pcscd.enable = true;
  security.tpm2 = {
    enable = true;
    pkcs11.enable = true;
    tctiEnvironment.enable = true;
  };

  users.users.root = {
    openssh.authorizedKeys.keyFiles = [
      (self + /keys/tokens/yk19666063/id_ecdsa_sha2_nistp384.pub)
      (self + /keys/tokens/yk19666099/id_ecdsa_sha2_nistp384.pub)
    ];
  };
}

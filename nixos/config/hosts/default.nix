inputs@{ self, nixpkgs, ... }:
{
  murozond = nixpkgs.lib.nixosSystem {
    specialArgs = inputs;
    modules = [
      ./murozond/configuration.nix
      ./murozond/hardware.nix
      ./murozond/disk.nix
    ];
  };
}

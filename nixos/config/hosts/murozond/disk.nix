{ disko, ... }:
{
  imports = [ disko.nixosModules.disko ];

  disko.devices.disk.disk-a = {
    type = "disk";
    device = "/dev/...";
    content = {
      type = "gpt";
      partitions = {
        ESP = {
          size = "512M";
          type = "EF00";
          content = {
            type = "filesystem";
            format = "vfat";
            mountpoint = "/boot-a";
          };
        };

        mdadm = {
          size = "100%";
          content = {
            type = "mdraid";
            name = "crypt";
          };
        };
      };
    };
  };

  disko.devices.disk.disk-b = {
    type = "disk";
    device = "/dev/...";
    content = {
      type = "gpt";
      partitions = {
        ESP = {
          size = "512M";
          type = "EF00";
          content = {
            type = "filesystem";
            format = "vfat";
            mountpoint = "/boot-b";
          };
        };

        mdadm = {
          size = "100%";
          content = {
            type = "mdraid";
            name = "crypt";
          };
        };
      };
    };
  };

  disko.devices.mdadm.crypt = {
    type = "mdadm";
    level = 1;
    content = {
      type = "luks";
      name = "crypted";
      content = {
        type = "lvm_pv";
        vg = "pool";
      };
    };
  };

  disko.devices.lvm_vg = {
    pool = {
      type = "lvm_vg";
      lvs.nix = {
        size = "25%";
        lvm_type = "raid1";
        content = {
          type = "filesystem";
          format = "ext4";
          mountpoint = "/nix";
        };
      };
      lvs.persist = {
        size = "75%";
        lvm_type = "raid1";
        content = {
          type = "filesystem";
          format = "ext4";
          mountpoint = "/mnt/persist";
        };
      };
    };
  };
}

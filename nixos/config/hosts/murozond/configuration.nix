{ self, ... }:
{
  imports = [
    (self + /nixos/modules/system/base.nix)
    (self + /nixos/modules/system/boot.nix)
    (self + /nixos/modules/system/age.nix)
  ];

  networking.hostName = "murozond";
  networking.hostId = "8425e349";
}

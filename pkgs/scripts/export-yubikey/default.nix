{
  writeScriptBin,
  yubikey-manager,
  age-plugin-yubikey,
  openssh,
  opensc,
}:
writeScriptBin "export-yubikey" ''
  SERIAL="$1"
  OUT="$2"

  mkdir -p $OUT

  ${age-plugin-yubikey}/bin/age-plugin-yubikey -i --serial $SERIAL --slot 1 > $OUT/age-identity.txt
  ${age-plugin-yubikey}/bin/age-plugin-yubikey -l --serial $SERIAL --slot 1 > $OUT/age-recipient.txt

  ${yubikey-manager}/bin/ykman piv keys export 9a $OUT/auth.pem
  ${yubikey-manager}/bin/ykman piv keys export 9c $OUT/cert.pem

  ${openssh}/bin/ssh-keygen -D ${opensc}/lib/opensc-pkcs11.so > $OUT/id_ecdsa_sha2_nistp384.pub 
''

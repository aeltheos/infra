{
  writeScriptBin,
  yubikey-manager,
  age-plugin-yubikey,
}:
writeScriptBin "enroll-yubikey" ''
  SERIAL="$1"
  TMPDIR="$(mktemp -d)"

  # disable all services which are not PIV
  ${yubikey-manager}/bin/ykman --device $SERIAL config usb -d OTP -f
  ${yubikey-manager}/bin/ykman --device $SERIAL config usb -d U2F -f
  ${yubikey-manager}/bin/ykman --device $SERIAL config usb -d FIDO2 -f
  ${yubikey-manager}/bin/ykman --device $SERIAL config usb -d OATH -f
  ${yubikey-manager}/bin/ykman --device $SERIAL config usb -d OPENPGP -f
  ${yubikey-manager}/bin/ykman --device $SERIAL config usb -d HSMAUTH -f

  # reset PIV
  ${yubikey-manager}/bin/ykman --device $SERIAL piv reset

  # configure piv PIN/PUK and age on slot 1 via yubikey-age-plugin
  ${age-plugin-yubikey}/bin/age-plugin-yubikey \
    --serial $SERIAL \
    --pin-policy once \
    --touch-policy cached \
    --slot 1 \
    --generate > /dev/null

  # create an auth key
  ${yubikey-manager}/bin/ykman --device $SERIAL piv keys generate \
    -a ECCP384 \
    --pin-policy ONCE \
    --touch-policy CACHED \
    9a $TMPDIR/public.pem

  ${yubikey-manager}/bin/ykman --device $SERIAL piv certificates generate \
    -s "CN=SSH" 9a $TMPDIR/public.pem

  # create a certification key
  ${yubikey-manager}/bin/ykman --device $SERIAL piv keys generate \
    -a ECCP384 \
    --pin-policy ONCE \
    --touch-policy ALWAYS \
    9c $TMPDIR/public.pem

  rm -r $TMPDIR
''

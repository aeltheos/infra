final: prev:
let
  callPackage = prev.lib.callPackageWith final;
in
{
  enroll-yubikey = callPackage ./scripts/enroll-yubikey { };

  export-yubikey = callPackage ./scripts/export-yubikey { };
}

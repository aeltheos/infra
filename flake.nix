{
  description = "aeltheos's nix infrastructure";
  inputs = {
    systems.url = "github:nix-systems/default";

    flake-utils = {
      url = "github:numtide/flake-utils";
      inputs.systems.follows = "systems";
    };

    nixpkgs.url = "github:nixos/nixpkgs/nixos-24.05";

    agenix = {
      url = "github:ryantm/agenix";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.systems.follows = "systems";
    };

    agenix-rekey = {
      url = "github:oddlama/agenix-rekey/126b4a5";
      inputs.flake-utils.follows = "flake-utils";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    lanzaboote = {
      url = "github:nix-community/lanzaboote/v0.4.1";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    impermanence.url = "github:nix-community/impermanence";

    disko = {
      url = "github:nix-community/disko/v1.7.0";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };
  outputs =
    inputs@{
      self,
      flake-utils,
      nixpkgs,
      agenix-rekey,
      ...
    }:
    flake-utils.lib.eachDefaultSystem (
      system:
      let
        pkgs = import nixpkgs {
          inherit system;
          overlays = [
            self.overlays.default
            agenix-rekey.overlays.default
          ];
        };
      in
      {
        formatter = pkgs.nixfmt-rfc-style;

        packages = {
          inherit (pkgs) enroll-yubikey export-yubikey;

          installer = import ./nixos/config/installer { inherit pkgs inputs; };
        };

        devShells.default = pkgs.mkShell {
          buildInputs = [
            pkgs.enroll-yubikey
            pkgs.export-yubikey
            pkgs.yubikey-manager
            pkgs.agenix-rekey
            pkgs.tree
          ];
        };
      }
    )
    // {
      overlays.default = import ./pkgs;

      agenix-rekey = agenix-rekey.configure {
        userFlake = self;
        nodes = self.nixosConfigurations;

        agePackage = p: p.age;
      };

      nixosConfigurations = import ./nixos/config/hosts inputs;
    };
}
